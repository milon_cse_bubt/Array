
<?php
  $x = array(5,9,10,15);//indexed array(for loop)
  $length = count($x);
  for ($i=0; $i < $length; $i++) { 
  	   echo $x[$i];
  	   echo "<br/>";
  }

?>

<?php
  $x = array(5,9,10,15);//indexed array(while loop)
  $length = count($x);
   $i = 0; 

 while($i < $length) {
    echo " $x[$i] <br>";
    $i++;
}
  
?>

<?php 
$x = array(5,9,10,15);//indexed array(do while loop)
$length = count($x);
$i = 0; 

do {
    echo "$x[$i] <br>";
    $i++;
} while ($i < $length);
?>

<?php
  $x = array(5,9,10,15);//indexed array(forach loop)
   foreach ($x as $key) {
   	echo "$key <br/>";
   }
?>
